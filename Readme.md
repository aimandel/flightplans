# Flight Plans

This is a collection of scripts (mostly R) that we find useful for mission planning and post processing sUAS imagery.

Alex Mandel, Arielle Rose, Emily Hurry, Sierra Mabanta 2018-2019
University of California, Davis

## PreFlight

* Planning a mission accounting for differences in camera field of view.
* Create importable flight plans for various apps from a geojson file. (in progress)

## PostFlight

* Sorting Sequioa RGB and Tiff to separate folders.
* GCP format conversion from GPS to Pix4d or Photoscan format.

See [uas-process](https://bitbucket.org/hijmans-lab/uas-process)

## PostProcessing

* Combine outputs to single images with multiple bands (Merge & Stack).
* Normalize results to same reflectance scale.
* Generate a preview (png) for every final product.

See [uas-process](https://bitbucket.org/hijmans-lab/uas-process)