# Code to compare different UAS sensors
# Given same Altitude (AGL), what overlap is needed in the flight planner to achieve the correct overlap required by a sensor.
# Alex Mandel 2018
# University of California, Davis



#### Conversions ####
# Convert between Radians and Degrees
rad2deg <- function(rad) {(rad * 180) / (pi)}
deg2rad <- function(deg) {(deg * pi) / (180)}

#### Defintions ####
# H,h = horizontal, the wide side of the camera image
# V,v = vertical, the narrow side of the camera image

## Defining Cameras
# TODO: turn camera definitions into a table and save (csv)
# X3
# All HFOV=81.25,VFOV=65.5,DFOV=94, RES=4000x3000
#source https://raw.githubusercontent.com/byzheng/missionplanner/master/camera.csv
x3 <- c(HFOV=81.25
        ,VFOV=65.5
        ,DFOV=94
        ,resh=4000
        ,resv=3000
        ,sensorh=6.17
        ,sensorv=4.55
        ,focal=3.6)

# Sequoia
# source micasense webpage
# RGB HFOV=63.9,VFOV=50.1,DFOV=73.5, RES=4608x3456, sensor=6.2x4.6mm, 4.9mm focal
seqRGB <- c(HFOV=63.9
            ,VFOV=50.1
            ,DFOV=73.5
            ,resh=4608
            ,resv=3456
            ,sensorh=6.2
            ,sensorv=4.6
            ,focal=4.9)
# Multi HFOV=61.9,VFOV=48.5,DFOV=73.7, RES=1280x960, sensor=4.8x3.6mm, 4mm focal
seqMult <- c(HFOV=61.9
             ,VFOV=48.5
             ,DFOV=73.5
             ,resh=1280
             ,resv=960
             ,sensorh=6.2
             ,sensorv=4.6
             ,focal=4.9)

# RedEdge camera specs
# HFOV listed in manual VFOV estimated by fov function
edgeMult <- c(HFOV=47.2
              ,VFOV=36.3
              ,DFOV=57.3
              ,resh=1280
              ,resv=960
              ,sensorh=4.8
              ,sensorv=3.6
              ,focal=5.5)



#### Functions ####  
fov <- function(Ha,Va,Df){
  #http://vrguy.blogspot.com/2013/04/converting-diagonal-field-of-view-and.html  
  Df <- deg2rad(Df)
  Da <- sqrt(Ha*Ha + Va*Va)
  #Df = atan(Da) * 2
  Hf <- atan( tan(Df/2) * (Ha/Da) ) * 2 
  Vf <- atan( tan(Df/2) * (Va/Da) ) * 2
  Hf <- rad2deg(Hf)
  Vf <- rad2deg(Vf)
  return(c(Hf,Vf))
}


distfromoverlap <- function(angle, agl, overlap){
  # Distance between photos to achieve overlap (whole numbers out of 100)
  # results in meters
  rangle <- deg2rad(angle/2)
  gd <- (2*agl)*(tan(rangle))
  ovd <- gd*((100-overlap)/100)
  return(ovd)
}

distbetween <- function(agl,overlap,focal,sensor){
  # given the overlap and sensor, determine the distance between images required
  # sensor is the sensor size is mm in the direction of overlap
  gend <- (agl/focal)*sensor*((100-overlap)/100)
  return(gend)
}

overbetween <- function(agl,focal,sensor,gend){
  # given the distance between images, calculate the overlap of a sensor
  overlap <- abs((100*(gend/((agl/focal)*sensor)))-100)
  return(overlap)
}

overlapfromdist <- function(angle, agl, dist){
  #Find the percent of image overlap give the FOV, alititude and distance between photos
  rangle <- deg2rad(angle/2)
  gd <- (2*agl)*(tan(rangle))
  overlap <- abs(((dist/gd)*100)-100)
  return(overlap)
}

#### Examples ####
main <- function(){
  # For the DJI X3 RGB camera, calculate the HFOV and VFOV of the camera
  Ha <- 4
  Va <- 3
  Df <- 94
  results <- fov(Ha,Va,Df)
  # Calculate the distance between images to achieve an overlap
  test1 <- distfromoverlap(48.5,60, 80)
  test2 <- seq(30,100, by=10)
  for(i in test2){
    print(distfromoverlap(48.5,i,70))
  }
  
}

maketable <- function(){
  #TODO: move to markdown file for easy printing of reference tables
  flights1 <- expand.grid(agl=seq(30,100, by=10), over=seq(70,95,by=5))
  flights <- expand.grid(agl=seq(30,100, by=10), over=seq(70,95,by=5))
  #given an overlap of the sequioa in the vertical direction
  #what distance interval should you set on the sequioa
  #flights$forwarddist <- mapply(distfromoverlap,flights$agl,flights$vfov,flights$over)
  #flights$sidedist <- mapply(distfromoverlap,flights$agl,flights$hfov,flights$over)
  flights$forwarddistSeq <- mapply(distbetween,flights$agl, flights$over,seqRGB["focal"],seqRGB["sensorv"] )
  flights$sidedistSeq <- mapply(distbetween,flights$agl, flights$over,seqRGB["focal"],seqRGB["sensorh"] )

  # Rededge
  flights$forwarddistRE <- mapply(distbetween,flights$agl, flights$over,edgeMult["focal"],edgeMult["sensorv"] )
  flights$sidedistRE <- mapply(distbetween,flights$agl, flights$over,edgeMult["focal"],edgeMult["sensorh"] )  
  
  #what overlap of the X3 should you tell the flight planner
  #flights$x3forward <- mapply(overlapfromdist,65.5,flights$agl,flights$forwarddist)
  flights$forwarddistx3 <- mapply(distbetween,flights$agl, flights$over,x3["focal"],x3["sensorv"] )
  flights$sidedistx3 <- mapply(distbetween,flights$agl, flights$over,x3["focal"],x3["sensorh"] )  
  flights$x3forwardlap <- mapply(overbetween,flights$agl, x3["focal"],x3["sensorh"],flights$forwarddistx3)
  
  #make a plot
  plot(range(flights$agl),range(flights$sidedistSeq),type="n", xlab="Height(m)", ylab="Distance Between Images")
  overs <- unique(flights$over)
  nflght <- length(overs)
  colors <- rainbow(nflght)
  #linetype <- c(1:nflght)
  #plotchar <- seq(18,18+nflght,1)
  
  for(i in 1:nflght) {
    flight <- subset(flights, over==overs[i])
    lines(flight$agl, flight$forwarddistSeq, type="b", col=colors[i])
    #with(flight, {
    #  abline(lm(sidedistSeq~agl), lty=2, col=colors[i])
    #})
  }
  
  title("Distance between Images", "Parrot Sequoia")
  legend("topleft", legend=overs, lty=1, col=colors, title="Overlap %")
}

RR_check <- function(){
  agl <- 80
  overlap <- 75 # min recommended by sensor
  # Required distance between shots to achieve overlap
  forwardDistRE <- distbetween(agl,overlap,edgeMult["focal"],edgeMult["sensorv"]) 
  sideDistRE <- distbetween(agl,overlap,edgeMult["focal"],edgeMult["sensorh"]) 
  forwardDistSeq <- distbetween(agl,overlap,seqMult["focal"],seqMult["sensorv"]) 
  sideDistSeq <- distbetween(agl,overlap,seqMult["focal"],seqMult["sensorh"])
  
  # Based on RedEdge requirement what percentage is needed for flight planner with x3?
  forwardlapX3 <- overbetween(agl,x3["focal"],x3["sensorv"],forwardDistRE)
  sidelapX3 <- overbetween(agl,x3["focal"],x3["sensorh"],sideDistRE)
  sideDistX3 <- distbetween(agl,overlap,x3["focal"],x3["sensorh"])  
}